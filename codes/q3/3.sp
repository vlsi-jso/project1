* Ehsan Jahangirzadeh 810194554
* CA1 part 2 c

**** Load libraries ****
.inc '45nm_PTM.txt'

**** Parameters ****
.param Lmin=100n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1V
+W=1u
.temp   25

**** Source Voltage ****
VSupply		Vs		GND  dc  Vdd
Vin 	    Vi	  GND  dc  0.8 
Vx		    Va	  GND

**** Subkits ****

**** Circuit ****
M1 		Vs 		Vi 		GND 	 	Va		nmos	w=W		L=Lmin 

**** Analysis ****

**** Measurements ****
.OP
.dc 	Vx 	-20	0	0.1
.PROBE DC i(M1)
.option post
.END
