* Ehsan Jahangirzadeh 810194554
* CA1 part 2 a

**** Load libraries ****
.inc '45nm_PTM.txt'

**** Parameters ****
.param Lmin=100n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1.1V
+W=1u
.temp   25

**** Source Voltage ****

VSupply		Vs		GND
Vin 	    Vi	  GND

**** Subkits ****

**** Circuit ****
M1 		Vs 		  Vi 		GND 	GND 	nmos	      w=W		L=Lmin 

**** Analysis ****

**** Measurements ****
.OP
.dc  VSupply	0	1	0.01	Vin 0.4 1 0.2
.PROBE DC i(M1)
.PROBE DC i(M2)
.option post
.END
