* Ehsan Jahangirzadeh 810194554
* CA1 part 1

**** Load libraries ****
.inc '45nm_PTM.txt'

**** Parameters ****
.param Lmin=45n
+slp=0.1p
+Out_T_DLY=12.95
+PRD=13
+Vdd=1.1V
+Wn='4*Lmin'
.temp   25

**** Source Voltage ****

VSupply		Vs		GND		DC		Vdd
Vin 	    Vi	  Vs	  0

**** Subkits ****

**** Circuit ****
M1 		Vs 		Vi 		GND 	 	nmos	w='4*Lmin'	L=Lmin 

**** Analysis ****

**** Measurements ****
.OP
.dc  VSupply	0	1	0.01
.PRINT GM=PAR('LX7(M1)')
.option post
.END
